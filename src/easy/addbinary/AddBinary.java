package easy.addbinary;

public class AddBinary {
    public static void main(String[] args) {
        String a = "1010";
        String b = "1011";

        String result = addBinary(a, b);

        System.out.println(result);
    }

    public static String addBinary(String a, String b) {
        int index_a = a.length() - 1;
        int index_b = b.length() - 1;
        int carry = 0;
        StringBuilder sum = new StringBuilder();

        while (index_a >= 0 || index_b >= 0 || carry == 1) {
            carry += index_a >= 0 ? a.charAt(index_a--) - '0' : 0;
            carry += index_b >= 0 ? b.charAt(index_b--) - '0' : 0;

            sum.insert(0, (char) (carry % 2 + '0'));
            carry /= 2;
        }

        return sum.toString();
    }
}
