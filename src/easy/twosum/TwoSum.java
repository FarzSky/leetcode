package easy.twosum;

import java.util.Arrays;
import java.util.HashMap;

public class TwoSum {
    public static void main(String[] args) {
        int[] nums = new int[] { 2,7,11,15 };
        int target = 9;

        int[] result = twoSum(nums, target);

        System.out.println(Arrays.toString(result));
    }

    public static int[] twoSum(int[] nums, int target) {
        if (nums.length == 0) return nums;

        HashMap<Integer, Integer> visited = new HashMap<>();

        for (int i=0; i<nums.length; i++) {
            int required = target - nums[i];
            if (visited.containsKey(required)) {
                return new int[] { visited.get(required), i };
            } else {
                visited.put(nums[i], i);
            }
        }

        return new int[0];
    }
}
