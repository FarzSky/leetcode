package easy.sqrt_x;

public class Sqrtx {
    public static void main(String[] args) {
        int x = 36;
        System.out.println(mySqrt(x));
    }

    public static int mySqrt(int x) {
        int i=1, counter = 0;
        while (x > 0) {
            x = x - i;
            i += 2;
            counter++;
        }
        return x < 0 ? counter - 1 : counter;
    }

    public static int mySqrtNewton(int x) {
        long r = x;
        while (r * r > x)
            r = (r + x/r) / 2;
        return (int) r;
    }
}
