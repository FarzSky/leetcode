package easy.plusone;

import java.util.Arrays;

public class PlusOne {
    public static void main(String[] args) {
        int[] digits = new int[] { 9 };

        int[] result = plusOne(digits);

        System.out.println(Arrays.toString(result));
    }

    public static int[] plusOne(int[] digits) {
        if (digits.length == 0) return new int[] { 1 };

        for (int i=digits.length - 1; i >= 0; i--) {
            if (digits[i] < 9) {
                digits[i]++;
                return digits;
            } else {
                digits[i] = 0;
            }
        }

        int[] newDigits = new int[digits.length + 1];
        newDigits[0] = 1;

        return newDigits;
    }
}
